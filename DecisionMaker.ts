module DecisionMaker {
    var domStrings = {
        addDecisionOptionButton: "addDecisionOptionButton",
        optionName: "optionName",
        decisionResult: "decisionResult",
        decisionOptions: "decisionOptions",
        makeDecisionButton: "makeDecisionButton",
        resetButton:"resetButton",
    
    }

    interface DecisionOption {
        optionName: string;
    }

    class DecisionOptionDOM
    {
        private domRoot:HTMLDivElement
        private static OPTION_INDEX = "optionIndex"

        constructor(optionName:string)
        {
            this.domRoot = <HTMLDivElement>document.createElement("div")

            this.domRoot.innerHTML = optionName
        }

        setOptionIndex(index:number)
        {
            this.domRoot.setAttribute(DecisionOptionDOM.OPTION_INDEX, index.toString())
        }

        getOptionIndex():number
        {
            return parseInt(this.domRoot.getAttribute(DecisionOptionDOM.OPTION_INDEX))
        }

        getName():string
        {
            return this.domRoot.innerHTML
        }

        getDOM():HTMLElement
        {
            return this.domRoot
        }
    }

    export class DecisionMaker {

        private addNewDecisionOptionBtn: HTMLButtonElement
        private makeDecisionButton: HTMLButtonElement
        private optionName: HTMLInputElement
        private decisionOptionsDiv: HTMLDivElement
        private resetButton:HTMLButtonElement
        private decisionResult:HTMLDivElement

        private decisionOptions: Array<DecisionOption>

        constructor() {
            this.decisionOptions = new Array()

            this.setupDOMReferences()

            this.addNewDecisionOptionBtn.onclick = (e) => {
                this.addNewDecisionOption()
            }

            this.makeDecisionButton.onclick = (e)=>{
                this.makeDecision()
            }

            this.resetButton.onclick = (e)=>{
                this.reset()
            }
        }

        private makeDecision()
        {
            var decisionIndexes = new Array<number>()
            var decisionIndex = -1

            for(var optionIndex = 0; optionIndex < this.decisionOptions.length; optionIndex++)
            {
                var option = this.decisionOptions[optionIndex]

                if(option!=null)
                {
                    decisionIndexes.push(optionIndex)
                }
            }

            if(decisionIndexes.length == 0)
            {
                return
            }

            var rand = ((Math.random() * 1000) | 0)

            decisionIndex = rand % decisionIndexes.length

            var decision = this.decisionOptions[decisionIndexes[decisionIndex]]

            this.decisionResult.innerHTML = decision.optionName
        }

        private reset()
        {
            this.decisionOptions = []

            while(this.decisionOptionsDiv.hasChildNodes())
            {
                this.decisionOptionsDiv.removeChild(this.decisionOptionsDiv.lastChild)
            }

            this.decisionResult.innerHTML = ""
        }


        private addNewDecisionOption() {

            if(this.optionName.value.length < 1)
            {
                return
            }

            var decisionOption = {
                optionName:this.optionName.value
            }

            this.decisionOptions.push(decisionOption)

            var decisionOptionDom = new DecisionOptionDOM(this.optionName.value)
            decisionOptionDom.setOptionIndex(this.decisionOptions.length - 1)

            this.decisionOptionsDiv.appendChild(decisionOptionDom.getDOM())

            this.optionName.value = ""
        }

        setupDOMReferences() {
            this.addNewDecisionOptionBtn
                = this.getDomObjectById<HTMLButtonElement>(domStrings.addDecisionOptionButton)

            this.makeDecisionButton
                = this.getDomObjectById<HTMLButtonElement>(domStrings.makeDecisionButton)

            this.optionName
                = this.getDomObjectById<HTMLInputElement>(domStrings.optionName)

            this.decisionOptionsDiv
                = this.getDomObjectById<HTMLDivElement>(domStrings.decisionOptions)

            this.resetButton
                = this.getDomObjectById<HTMLButtonElement>(domStrings.resetButton)

            this.decisionResult 
                = this.getDomObjectById<HTMLDivElement>(domStrings.decisionResult)
        }

        getDomObjectById<T extends HTMLElement>(id: string): T {
            return <T>document.getElementById(id)
        }
    }
}



var main:DecisionMaker.DecisionMaker

window.onload = (e) => {
    main = new DecisionMaker.DecisionMaker()    
}