var DecisionMaker;
(function (DecisionMaker_1) {
    var domStrings = {
        addDecisionOptionButton: "addDecisionOptionButton",
        optionName: "optionName",
        decisionResult: "decisionResult",
        decisionOptions: "decisionOptions",
        makeDecisionButton: "makeDecisionButton",
        resetButton: "resetButton"
    };
    var DecisionOptionDOM = (function () {
        function DecisionOptionDOM(optionName) {
            this.domRoot = document.createElement("div");
            this.domRoot.innerHTML = optionName;
        }
        DecisionOptionDOM.prototype.setOptionIndex = function (index) {
            this.domRoot.setAttribute(DecisionOptionDOM.OPTION_INDEX, index.toString());
        };
        DecisionOptionDOM.prototype.getOptionIndex = function () {
            return parseInt(this.domRoot.getAttribute(DecisionOptionDOM.OPTION_INDEX));
        };
        DecisionOptionDOM.prototype.getName = function () {
            return this.domRoot.innerHTML;
        };
        DecisionOptionDOM.prototype.getDOM = function () {
            return this.domRoot;
        };
        DecisionOptionDOM.OPTION_INDEX = "optionIndex";
        return DecisionOptionDOM;
    }());
    var DecisionMaker = (function () {
        function DecisionMaker() {
            var _this = this;
            this.decisionOptions = new Array();
            this.setupDOMReferences();
            this.addNewDecisionOptionBtn.onclick = function (e) {
                _this.addNewDecisionOption();
            };
            this.makeDecisionButton.onclick = function (e) {
                _this.makeDecision();
            };
            this.resetButton.onclick = function (e) {
                _this.reset();
            };
        }
        DecisionMaker.prototype.makeDecision = function () {
            var decisionIndexes = new Array();
            var decisionIndex = -1;
            for (var optionIndex = 0; optionIndex < this.decisionOptions.length; optionIndex++) {
                var option = this.decisionOptions[optionIndex];
                if (option != null) {
                    decisionIndexes.push(optionIndex);
                }
            }
            if (decisionIndexes.length == 0) {
                return;
            }
            var rand = ((Math.random() * 1000) | 0);
            decisionIndex = rand % decisionIndexes.length;
            var decision = this.decisionOptions[decisionIndexes[decisionIndex]];
            this.decisionResult.innerHTML = decision.optionName;
        };
        DecisionMaker.prototype.reset = function () {
            this.decisionOptions = [];
            while (this.decisionOptionsDiv.hasChildNodes()) {
                this.decisionOptionsDiv.removeChild(this.decisionOptionsDiv.lastChild);
            }
            this.decisionResult.innerHTML = "";
        };
        DecisionMaker.prototype.addNewDecisionOption = function () {
            if (this.optionName.value.length < 1) {
                return;
            }
            var decisionOption = {
                optionName: this.optionName.value
            };
            this.decisionOptions.push(decisionOption);
            var decisionOptionDom = new DecisionOptionDOM(this.optionName.value);
            decisionOptionDom.setOptionIndex(this.decisionOptions.length - 1);
            this.decisionOptionsDiv.appendChild(decisionOptionDom.getDOM());
            this.optionName.value = "";
        };
        DecisionMaker.prototype.setupDOMReferences = function () {
            this.addNewDecisionOptionBtn
                = this.getDomObjectById(domStrings.addDecisionOptionButton);
            this.makeDecisionButton
                = this.getDomObjectById(domStrings.makeDecisionButton);
            this.optionName
                = this.getDomObjectById(domStrings.optionName);
            this.decisionOptionsDiv
                = this.getDomObjectById(domStrings.decisionOptions);
            this.resetButton
                = this.getDomObjectById(domStrings.resetButton);
            this.decisionResult
                = this.getDomObjectById(domStrings.decisionResult);
        };
        DecisionMaker.prototype.getDomObjectById = function (id) {
            return document.getElementById(id);
        };
        return DecisionMaker;
    }());
    DecisionMaker_1.DecisionMaker = DecisionMaker;
})(DecisionMaker || (DecisionMaker = {}));
var main;
window.onload = function (e) {
    main = new DecisionMaker.DecisionMaker();
};
